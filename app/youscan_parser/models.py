from django.db import models


class Theme(models.Model):
    youscan_id = models.PositiveBigIntegerField()
    name = models.CharField(max_length=300)


class Mention(models.Model):
    youscan_id = models.PositiveBigIntegerField()
    data = models.JSONField()
    published = models.DateTimeField()
    theme = models.ForeignKey(Theme, related_name='mentions', on_delete=models.CASCADE)

