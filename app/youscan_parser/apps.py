from django.apps import AppConfig


class YouscanParserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'youscan_parser'
