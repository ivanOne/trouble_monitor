from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.views.generic import ListView
from pure_pagination import PaginationMixin

from youscan_parser.models import Theme, Mention


class ThemeListView(ListView):
    paginate_by = 0
    context_object_name = 'themes'
    template_name = 'themes.html'
    queryset = Theme.objects.all()


class MentionsListView(PaginationMixin, ListView):
    paginate_by = 100
    context_object_name = 'mentions'
    template_name = 'mentions.html'
    queryset = Mention.objects.all().order_by('-published')

    def get_queryset(self):
        theme = get_object_or_404(Theme, id=self.kwargs['theme'])
        return self.queryset.filter(theme=theme)
