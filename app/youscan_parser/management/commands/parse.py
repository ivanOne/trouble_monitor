import time
from dataclasses import dataclass
import datetime
from json import JSONDecodeError
from typing import List, Dict

import math
import requests
from django.core.management.base import BaseCommand
from django.utils.timezone import utc

from youscan_parser.models import Theme, Mention


class ParserException(Exception):
    pass


class ParserAuthException(ParserException):
    pass


class ParserResponseException(ParserException):
    pass


class Command(BaseCommand):

    def auth(self):
        self.print_message('Авторизация')
        response = requests.post(
            'https://next.youscan.io/api/Token?',
            data={
                'grant_type': 'password',
                'username': 'ogorodov.ka@zhcom.ru',
                'password': '123456'
            }
        )
        if response.status_code == 200:
            try:
                data = response.json()
                return f'{data["token_type"]} {data["access_token"]}'
            except (JSONDecodeError, KeyError):
                raise ParserAuthException('Неудачная попытка авторизации - ошибка обработки ответа.')
        raise ParserAuthException(f'Неудачная попытка авторизации, код ответа - {response.status_code}')

    @staticmethod
    def get_themes(key: str) -> Dict:
        response = requests.get(
            'https://next.youscan.io/api/themes',
            headers={
                'authorization': key
            }
        )
        created_themes = 0
        if response.status_code == 200:
            try:
                data = response.json()
                result = {}
                for theme_data in data['themes']:
                    theme, created = Theme.objects.update_or_create(
                        youscan_id=theme_data['id'],
                        defaults={'name': theme_data['name']}
                    )
                    if created:
                        created_themes += 1
                    result[theme_data['id']] = theme
                return result, created_themes
            except (JSONDecodeError, KeyError):
                raise ParserResponseException(f'Ошибка запроса тем - не удалось обработать ответ.')
        raise ParserResponseException(f'Ошибка запроса тем, код ответа - {response.status_code}')

    def get_mentions(
            self,
            theme_id: int,
            token: str,
            skip: int = 0,
            take: int = 200
    ):
        url = f'https://next.youscan.io/api/themes/{theme_id}/mentions/list'
        today = datetime.date.today()
        data = {
            'from': datetime.date(year=today.year, month=1, day=1).isoformat(),
            'to': today.isoformat()
        }
        params = {
            'orderBy': 'published',
            'skip': skip,
            'take': take
        }

        headers = {
            'authorization': token
        }

        self.print_message(f'Запрос упоминаний url - {url}, data - {data}, params - {params}')
        response = requests.post(url, params=params, data=data, headers=headers)

        if response.status_code == 200:
            try:
                return response.json()
            except JSONDecodeError:
                raise ParserResponseException('Ошибка при обработке ответа')
        return None

    def parse_datetime(self, dt_str):
        dt_str = dt_str.replace('Z', '')
        result = dt_str.strip().split(".")
        if len(result) == 1:
            return datetime.datetime.strptime(dt_str, '%Y-%m-%dT%H:%M:%S')
        (dt, mSecs) = result[0], result[1]
        dt = datetime.datetime(*time.strptime(dt, "%Y-%m-%dT%H:%M:%S")[0:6])
        mSeconds = datetime.timedelta(microseconds=int(mSecs))
        return dt + mSeconds

    def handle_mentions(self, mentions, theme):
        created_mentions = 0
        mentions_ids = set([mention['id'] for mention in mentions['mentions']])
        mentions_db_ids = Mention.objects.filter(youscan_id__in=mentions_ids).values_list('youscan_id', flat=True)
        ids_for_create = mentions_ids.difference(mentions_db_ids)

        if ids_for_create:
            for mention in mentions['mentions']:
                if mention['id'] in ids_for_create:
                    published = utc.localize(self.parse_datetime(mention['publishedUtc']))

                    Mention.objects.create(
                        youscan_id=mention['id'],
                        data=mention,
                        published=published,
                        theme=theme
                    )
                    created_mentions += 1
        return created_mentions

    @staticmethod
    def print_message(message):
        print(f'-------{message}-------')

    def handle(self, *args, **options):
        self.print_message('Запуск парсера')
        try:
            auth_token = self.auth()
            self.print_message('Авторизация пройдена')
        except ParserAuthException as e:
            self.print_message(f'Ошибка авторизации - {e}')
            return

        self.print_message('Получение и создание тем')
        try:
            themes, new_themes = self.get_themes(auth_token)
            self.print_message(f'Темы получены, новых тем - {new_themes}')
        except ParserResponseException as e:
            self.print_message(f'Ошибка получения тем - {e}')
            return

        if themes:
            self.print_message('Сбор упоминаний')
            created_mentions_count = 0
            failed_requests = 0
            for theme_id, theme in themes.items():
                self.print_message(f'Сбор упоминаний для темы {theme.name}')
                try:
                    mentions = self.get_mentions(theme_id, auth_token)
                except ParserException as e:
                    self.print_message(
                        f'Ошибка обработки первой страницы упоминаний {e}, переключение на следующую тему'
                    )
                    failed_requests += 1
                    continue

                if mentions:
                    total_mentions = mentions['total']
                    mentions_per_page = len(mentions['mentions'])

                    created_mentions_count += self.handle_mentions(mentions, theme)

                    total_pages = math.ceil(total_mentions / mentions_per_page)
                    for num in range(total_pages):
                        skip = (num + 1) * mentions_per_page
                        try:
                            mentions = self.get_mentions(theme_id, auth_token, skip=skip)
                        except ParserException as e:
                            self.print_message(
                                f'Ошибка обработки страницы упоминаний {e}'
                            )
                            failed_requests += 1
                            continue
                        if mentions:
                            created_mentions_count += self.handle_mentions(mentions, theme)
            self.print_message(f'Обработка упоминаний завершена, новых упоминаний - {created_mentions_count}, '
                               f'ошибок - {failed_requests}')
        self.print_message('Парсер завершил работу')
