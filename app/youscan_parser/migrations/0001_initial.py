# Generated by Django 3.2.7 on 2021-09-30 05:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('youscan_id', models.PositiveBigIntegerField()),
                ('name', models.CharField(max_length=300)),
            ],
        ),
        migrations.CreateModel(
            name='Mention',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('youscan_id', models.PositiveBigIntegerField()),
                ('data', models.JSONField()),
                ('published', models.DateTimeField()),
                ('theme', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mentions', to='youscan_parser.theme')),
            ],
        ),
    ]
