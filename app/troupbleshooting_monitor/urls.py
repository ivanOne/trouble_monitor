from django.contrib import admin
from django.urls import path

from youscan_parser.views import ThemeListView, MentionsListView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', ThemeListView.as_view()),
    path('theme/<int:theme>/mentions/', MentionsListView.as_view()),
]
